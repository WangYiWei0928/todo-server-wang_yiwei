class Todo < ApplicationRecord
  MAX_TITLE_LENGTH = 100
  MAX_DETAIL_LENGTH = 1000

  validates :title, presence: true, length: { maximum: MAX_TITLE_LENGTH }
  validates :detail, length: { maximum: MAX_DETAIL_LENGTH }
end
