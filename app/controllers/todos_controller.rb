class TodosController < ApplicationController
  before_action :set_todo, only: [:update, :destroy]

  NO_ERROR_CODE = 0
  INDEX_ERROR_CODE = 3
  CREATE_ERROR_CODE = 4
  UPDATE_ERROR_CODE = 5
  DESTROY_ERROR_CODE = 6

  def index
    todos = Todo.select(:id, :title, :detail, :date).order(:date)
    render_success({ todos: todos })
  rescue ActiveRecord::ActiveRecordError
    render json: { error_code: INDEX_ERROR_CODE, error_message: '一覧の取得に失敗しました' }, status: :internal_server_error
  end

  def create
    Todo.create!(permit_params)
    render_success
  rescue ActiveRecord::RecordInvalid
    render_bad_request
  rescue ActiveRecord::ActiveRecordError
    render json: { error_code: CREATE_ERROR_CODE, error_message: '登録に失敗しました' }, status: :internal_server_error
  end

  def update
    @todo.update!(permit_params)
    render_success
  rescue ActiveRecord::RecordInvalid
    render_bad_request
  rescue ActiveRecord::ActiveRecordError
    render json: { error_code: UPDATE_ERROR_CODE, error_message: '更新に失敗しました' }, status: :internal_server_error
  end

  def destroy
    @todo.destroy!
    render_success
  rescue ActiveRecord::ActiveRecordError
    render json: { error_code: DESTROY_ERROR_CODE, error_message: '削除に失敗しました' }, status: :internal_server_error
  end

  private

  def permit_params
    params.permit(:title, :detail, :date)
  end

  def set_todo
    @todo = Todo.find(params[:id])
  end

  def render_success(params = {})
    base_params = { error_code: NO_ERROR_CODE, error_message: '' }
    render json: base_params.merge(params), status: :ok
  end
end
