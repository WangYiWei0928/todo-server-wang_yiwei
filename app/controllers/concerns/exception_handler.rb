module ExceptionHandler
  extend ActiveSupport::Concern

  INTERNAL_SERVER_ERROR_CODE = 1
  BAD_REQUEST_CODE = 2

  included do
    rescue_from Exception, with: :render_internal_server_error
    rescue_from ActiveRecord::ActiveRecordError, with: :render_bad_request
  end

  def render_internal_server_error
    render json: { error_code: INTERNAL_SERVER_ERROR_CODE, error_message: 'サーバー内で不明なエラーが発生しました' }, status: :internal_server_error
  end

  def render_bad_request
    render json: { error_code: BAD_REQUEST_CODE, error_message: 'リクエストの形式が不正です' }, status: :bad_request
  end
end
