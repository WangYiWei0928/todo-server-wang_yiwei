require 'rails_helper'

RSpec.describe 'Todos', type: :request do
  # initialize test data
  let!(:todos) { create_list(:todo, 10) }
  let(:todo_id) { todos.first.id }

  # Test suite for GET /todos
  describe 'GET /todos' do
    example '一覧の取得成功' do
      get '/todos'

      expect(response.status).to eq 200
      expect(json).to be_present
      expect(json['todos'].size).to eq 10
      expect(json['error_code']).to eq 0
    end

    example '一覧取得失敗' do
      allow(Todo).to receive(:select).and_raise(ActiveRecord::ActiveRecordError)
      get '/todos'

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 3
      expect(json['error_message']).to eq '一覧の取得に失敗しました'
    end

    context 'サーバー内で不明なエラーが発生した場合' do
      example '一覧取得の失敗' do
        allow(Todo).to receive(:select).and_raise(Exception)
        get '/todos'

        expect(response.status).to eq 500
        expect(json).to be_present
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  # Test suite for POST /todos
  describe 'POST /todos' do
    let(:valid_attributes) { { title: 'title', detail: 'test', date: '2020-11-02' } }
    let(:invalid_attributes) { { title: '' } }

    example 'TODO登録成功' do
      post '/todos', params: valid_attributes

      expect(response.status).to eq 200
      expect(json['error_code']).to eq 0
      expect(json['error_message']).to eq ''
    end

    context 'リクエストの形式が不正の場合' do
      example 'TODO登録失敗' do
        post '/todos', params: invalid_attributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'タイトルが丁度100文字' do
      let(:params) { { title: 'a' * 100 } }

      example 'TODO登録成功' do
        post '/todos', params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'ディテールが丁度1000文字' do
      let(:params) { { title: 'title', detail: 'a' * 1000 } }

      example 'TODO登録成功' do
        post '/todos', params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'タイトルが制限文字超えた場合' do
      let(:params) { { title: 'a' * 101 } }

      example 'TODO登録失敗' do
        post '/todos', params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'ディテールが制限文字超えた場合' do
      let(:params) { { title: 'title', detail: 'a' * 1001 } }

      example 'TODO登録失敗' do
        post '/todos', params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    example 'TODO登録失敗' do
      allow(Todo).to receive(:create!).and_raise(ActiveRecord::ActiveRecordError)
      post '/todos', params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 4
      expect(json['error_message']).to eq '登録に失敗しました'
    end

    context 'サーバー内で不明なエラーが発生した場合' do
      example 'TODO登録失敗' do
        allow(Todo).to receive(:create!).and_raise(Exception)
        post '/todos', params: valid_attributes

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'PUT /todos/:id' do
    let(:valid_attributes) { { title: 'title', detail: 'test', date: '2020-11-02' } }
    let(:invalid_attributes) { { title: '' } }

    context 'データが存在する場合' do
      example 'TODO更新成功' do
        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'タイトルが丁度100文字' do
      let(:params) { { title: 'a' * 100 } }

      example 'TODO更新成功' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'ディテールが丁度1000文字' do
      let(:params) { { detail: 'a' * 1000 } }

      example 'TODO更新成功' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'タイトルが制限文字超えた場合' do
      let(:params) { { title: 'a' * 101 } }

      example 'TODO更新失敗' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'ディテールが制限文字超えた場合' do
      let(:params) { { title: 'title', detail: 'a' * 1001 } }

      example 'TODO更新失敗' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'リクエストの形式が不正の場合' do
      example 'TODO更新失敗' do
        put "/todos/#{todo_id}", params: invalid_attributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'サーバー内で不明なエラーが発生した場合' do
      example 'TODO更新失敗' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(Exception)
        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end

    example 'TODO更新失敗' do
      allow_any_instance_of(Todo).to receive(:update!).and_raise(ActiveRecord::ActiveRecordError)
      put "/todos/#{todo_id}", params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 5
      expect(json['error_message']).to eq '更新に失敗しました'
    end
  end

  describe 'DELETE /todos/:id' do
    context 'データが存在する場合' do
      example 'TODO削除成功' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    example 'TODO削除失敗' do
      allow_any_instance_of(Todo).to receive(:destroy!).and_raise(ActiveRecord::ActiveRecordError)
      delete "/todos/#{todo_id}"

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 6
      expect(json['error_message']).to eq '削除に失敗しました'
    end

    context 'データが存在しない場合' do
      before { Todo.find(todo_id).destroy }
      example 'TODO削除失敗' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'サーバー内で不明なエラーが発生した場合' do
      example 'TODO削除失敗' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(Exception)
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end
end
